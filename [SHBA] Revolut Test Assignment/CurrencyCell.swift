//
//  CurrencyCell.swift
//  [SHBA] Revolut Test Assignment
//
//  Created by Artur Shelekhov-Balchunas on 16/10/2018.
//  Copyright © 2018 Artur Shelekhov-Balchunas. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var currencyTitle: UILabel!
    @IBOutlet weak var currencyAmount: UITextField!
    
    var tableView: UITableView? {
        get {
            let view = self.superview
            if let tableView = view as? UITableView {
                return tableView
            } else {
                return view?.superview as? UITableView
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.interestedCurrency, object: self, userInfo: [
            "code": self.currencyTitle.text!
        ])
        if let indexPath = self.tableView?.indexPath(for: self) {
            self.tableView!.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.top)
        }
    }
    
    @IBAction func textFieldDidChange(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.recalculateAmounts, object: self, userInfo: [
            "newAmount": Double(self.currencyAmount.text!) ?? 0
        ])
    }
    
}
