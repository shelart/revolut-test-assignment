//
//  NotificationName.swift
//  [SHBA] Revolut Test Assignment
//
//  Created by Artur Shelekhov-Balchunas on 18/10/2018.
//  Copyright © 2018 Artur Shelekhov-Balchunas. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let interestedCurrency = Notification.Name("interestedCurrency")
    static let recalculateAmounts = Notification.Name("recalculateAmounts")
}
