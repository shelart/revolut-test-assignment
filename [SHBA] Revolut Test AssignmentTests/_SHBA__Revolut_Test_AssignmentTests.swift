//
//  _SHBA__Revolut_Test_AssignmentTests.swift
//  [SHBA] Revolut Test AssignmentTests
//
//  Created by Artur Shelekhov-Balchunas on 16/10/2018.
//  Copyright © 2018 Artur Shelekhov-Balchunas. All rights reserved.
//

import XCTest
import Alamofire
import Mockingjay
import Nimble
@testable import _SHBA__Revolut_Test_Assignment

class _SHBA__Revolut_Test_AssignmentTests: XCTestCase {
    
    let url = "https://revolut.duckdns.org/latest?base=EUR"
    let headers = [
        "Server": "nginx/1.14.0",
        "Content-Type": "application/json",
        "Connection": "keep-alive",
        "Vary": "Origin",
        "X-Content-Type-Options": "nosniff",
        "Strict-Transport-Security": "max-age=604800",
    ]

    override func setUp() {
    }

    override func tearDown() {
    }

    func testResponseProper() {
        let promise = expectation(description: "Response Proper")
        
        let response = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6241,\"CAD\":1.5411,\"GBP\":0.90254,\"NZD\":1.7717,\"RUB\":79.956,\"USD\":1.169}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        let expectedRatesData = [
            CurrencyData(code: "AUD", rateToEuro: 1.6241, amount: 1.6241),
            CurrencyData(code: "CAD", rateToEuro: 1.5411, amount: 1.5411),
            CurrencyData(code: "EUR", rateToEuro: 1, amount: 1),
            CurrencyData(code: "GBP", rateToEuro: 0.90254, amount: 0.90254),
            CurrencyData(code: "NZD", rateToEuro: 1.7717, amount: 1.7717),
            CurrencyData(code: "RUB", rateToEuro: 79.956, amount: 79.956),
            CurrencyData(code: "USD", rateToEuro: 1.169, amount: 1.169),
        ]
        let expectedBaseCurrencyCode = "EUR"
        let within = 0.01
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let actualRates = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                
                expect(interestingCurrencyCode).to(match("EUR"))
                expect(interestingCurrencyAmount).to(beCloseTo(1, within: within))
                expect(actualRates.baseCurrencyCode).to(match(expectedBaseCurrencyCode))
                expect(actualRates.ratesData).to(haveCount(expectedRatesData.count))
                for expectedCurrencyData in expectedRatesData {
                    expect(actualRates.ratesData).to(containElementSatisfying({currencyData in
                        if (expectedCurrencyData.code == currencyData.code) {
                            print("Delta for \(currencyData.code): \(abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro))")
                        }
                        return
                            (expectedCurrencyData.code == currencyData.code)
                            && (abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro) <= within)
                            && (abs(expectedCurrencyData.amount - currencyData.amount) <= within)
                    }))
                }
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseWrongOrder() {
        let promise = expectation(description: "Response Wrong Order")
        
        let response = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"CAD\":1.5411,\"NZD\":1.7717,\"AUD\":1.6241,\"RUB\":79.956,\"USD\":1.169,\"GBP\":0.90254}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        let expectedRatesData = [
            CurrencyData(code: "AUD", rateToEuro: 1.6241, amount: 1.6241),
            CurrencyData(code: "CAD", rateToEuro: 1.5411, amount: 1.5411),
            CurrencyData(code: "EUR", rateToEuro: 1, amount: 1),
            CurrencyData(code: "GBP", rateToEuro: 0.90254, amount: 0.90254),
            CurrencyData(code: "NZD", rateToEuro: 1.7717, amount: 1.7717),
            CurrencyData(code: "RUB", rateToEuro: 79.956, amount: 79.956),
            CurrencyData(code: "USD", rateToEuro: 1.169, amount: 1.169),
        ]
        let expectedBaseCurrencyCode = "EUR"
        let within = 0.01
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let actualRates = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                
                expect(interestingCurrencyCode).to(match("EUR"))
                expect(interestingCurrencyAmount).to(beCloseTo(1, within: within))
                expect(actualRates.baseCurrencyCode).to(match(expectedBaseCurrencyCode))
                expect(actualRates.ratesData).to(haveCount(expectedRatesData.count))
                for expectedCurrencyData in expectedRatesData {
                    expect(actualRates.ratesData).to(containElementSatisfying({currencyData in
                        if (expectedCurrencyData.code == currencyData.code) {
                            print("Delta for \(currencyData.code): \(abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro))")
                        }
                        return
                            (expectedCurrencyData.code == currencyData.code)
                            && (abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro) <= within)
                            && (abs(expectedCurrencyData.amount - currencyData.amount) <= within)
                    }))
                }
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseExplicitDuplicate() {
        let promise = expectation(description: "Response Explicit Dup")
        
        let response = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6241,\"CAD\":1.5411,\"CAD\":1.5711,\"GBP\":0.90254,\"NZD\":1.7717,\"RUB\":79.956,\"USD\":1.169}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        let expectedRatesData = [
            CurrencyData(code: "AUD", rateToEuro: 1.6241, amount: 1.6241),
            CurrencyData(code: "CAD", rateToEuro: 1.5411, amount: 1.5411),
            CurrencyData(code: "EUR", rateToEuro: 1, amount: 1),
            CurrencyData(code: "GBP", rateToEuro: 0.90254, amount: 0.90254),
            CurrencyData(code: "NZD", rateToEuro: 1.7717, amount: 1.7717),
            CurrencyData(code: "RUB", rateToEuro: 79.956, amount: 79.956),
            CurrencyData(code: "USD", rateToEuro: 1.169, amount: 1.169),
        ]
        let expectedBaseCurrencyCode = "EUR"
        let within = 0.01

        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let actualRates = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                
                expect(interestingCurrencyCode).to(match("EUR"))
                expect(interestingCurrencyAmount).to(beCloseTo(1, within: within))
                expect(actualRates.baseCurrencyCode).to(match(expectedBaseCurrencyCode))
                expect(actualRates.ratesData).to(haveCount(expectedRatesData.count))
                for expectedCurrencyData in expectedRatesData {
                    expect(actualRates.ratesData).to(containElementSatisfying({currencyData in
                        if (expectedCurrencyData.code == currencyData.code) {
                            print("Delta for \(currencyData.code): \(abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro))")
                        }
                        return
                            (expectedCurrencyData.code == currencyData.code)
                            && (abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro) <= within)
                            && (abs(expectedCurrencyData.amount - currencyData.amount) <= within)
                    }))
                }
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseImplicitDuplicate() {
        let promise = expectation(description: "Response Implicit Dup (Base Dupped)")
        
        let response = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6241,\"CAD\":1.5411,\"EUR\":1,\"GBP\":0.90254,\"NZD\":1.7717,\"RUB\":79.956,\"USD\":1.169}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        let expectedRatesData = [
            CurrencyData(code: "AUD", rateToEuro: 1.6241, amount: 1.6241),
            CurrencyData(code: "CAD", rateToEuro: 1.5411, amount: 1.5411),
            CurrencyData(code: "EUR", rateToEuro: 1, amount: 1),
            CurrencyData(code: "GBP", rateToEuro: 0.90254, amount: 0.90254),
            CurrencyData(code: "NZD", rateToEuro: 1.7717, amount: 1.7717),
            CurrencyData(code: "RUB", rateToEuro: 79.956, amount: 79.956),
            CurrencyData(code: "USD", rateToEuro: 1.169, amount: 1.169),
        ]
        let expectedBaseCurrencyCode = "EUR"
        let within = 0.01

        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let actualRates = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                
                expect(interestingCurrencyCode).to(match("EUR"))
                expect(interestingCurrencyAmount).to(beCloseTo(1, within: within))
                expect(actualRates.baseCurrencyCode).to(match(expectedBaseCurrencyCode))
                expect(actualRates.ratesData).to(haveCount(expectedRatesData.count))
                for expectedCurrencyData in expectedRatesData {
                    expect(actualRates.ratesData).to(containElementSatisfying({currencyData in
                        if (expectedCurrencyData.code == currencyData.code) {
                            print("Delta for \(currencyData.code): \(abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro))")
                            print("Delta for \(currencyData.code): \(abs(expectedCurrencyData.amount - currencyData.amount))")
                        }
                        return
                            (expectedCurrencyData.code == currencyData.code)
                            && (abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro) <= within)
                            && (abs(expectedCurrencyData.amount - currencyData.amount) <= within)
                    }))
                }
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseBaseRateNotEqualToOne1() {
        let promise = expectation(description: "Response EUR rate is not 1")
        
        let response = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6241,\"CAD\":1.5411,\"EUR\":1.11,\"GBP\":0.90254,\"NZD\":1.7717,\"RUB\":79.956,\"USD\":1.169}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let _ = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                XCTFail("Expected exception but passed")
            } catch RatesError.dupBaseWithWrongRate {
                XCTAssert(true)
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseBaseRateNotEqualToOne2() {
        let promise = expectation(description: "Response RUB rate is not 1")
        
        let response = "{\"base\":\"RUB\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.0203,\"CAD\":0.0193,\"EUR\":0.0125,\"GBP\":0.0113,\"NZD\":0.0222,\"RUB\":79.956,\"USD\":0.0146}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let _ = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                XCTFail("Expected exception but passed")
            } catch RatesError.dupBaseWithWrongRate {
                XCTAssert(true)
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseUnexpectedBase() {
        let promise = expectation(description: "Response base is RUB instead of EUR")
        
        let response = "{\"base\":\"RUB\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.0203,\"CAD\":0.0193,\"EUR\":0.012507,\"GBP\":0.0113,\"NZD\":0.0222,\"USD\":0.0146}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        let expectedRatesData = [
            CurrencyData(code: "AUD", rateToEuro: 1.6241, amount: 0.0203),
            CurrencyData(code: "CAD", rateToEuro: 1.5411, amount: 0.0193),
            CurrencyData(code: "EUR", rateToEuro: 1, amount: 0.012507),
            CurrencyData(code: "GBP", rateToEuro: 0.90254, amount: 0.0113),
            CurrencyData(code: "NZD", rateToEuro: 1.7717, amount: 0.0222),
            CurrencyData(code: "RUB", rateToEuro: 79.956, amount: 1),
            CurrencyData(code: "USD", rateToEuro: 1.169, amount: 0.0146),
        ]
        let expectedBaseCurrencyCode = "RUB"
        let within = 0.01

        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let actualRates = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                
                expect(interestingCurrencyCode).to(match("RUB"))
                expect(interestingCurrencyAmount).to(beCloseTo(1, within: within))
                expect(actualRates.baseCurrencyCode).to(match(expectedBaseCurrencyCode))
                expect(actualRates.ratesData).to(haveCount(expectedRatesData.count))
                for expectedCurrencyData in expectedRatesData {
                    expect(actualRates.ratesData).to(containElementSatisfying({currencyData in
                        if (expectedCurrencyData.code == currencyData.code) {
                            print("Delta for \(currencyData.code): \(abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro))")
                        }
                        return
                            (expectedCurrencyData.code == currencyData.code)
                            && (abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro) <= within)
                            && (abs(expectedCurrencyData.amount - currencyData.amount) <= within)
                    }))
                }
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseWrongBase() {
        let promise = expectation(description: "Response base is RUB, but missing EUR rate")
        
        let response = "{\"base\":\"RUB\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.0203,\"CAD\":0.0193,\"GBP\":0.0113,\"NZD\":0.0222,\"USD\":0.0146}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let _ = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                XCTFail("Expected exception but passed")
            } catch RatesError.noEuroRate {
                XCTAssert(true)
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseInvalidJSON() {
        let promise = expectation(description: "Response is invalid JSON")
        
        let response = "{\"base\":\"RUB\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.0203,\"CAD\":0.0193,\"EUR\":0.012507,,\"GBP\":0.0113,\"NZD\":0.0222,\"USD\":0.0146}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let _ = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                XCTFail("Expected exception but passed")
            } catch RatesResponseError.invalidJsonNotConvertable {
                XCTAssert(true)
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseWrongRatesJSON() {
        let promise = expectation(description: "Response is JSON with unexpectedly formatted 'rates''")
        
        let response = "{\"base\":\"RUB\",\"date\":\"2018-09-06\",\"rates\":[{\"AUD\":0.0203},{\"CAD\":0.0193},{\"EUR\":0.012507},{\"GBP\":0.0113},{\"NZD\":0.0222},{\"USD\":0.0146}]}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let _ = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                XCTFail("Expected exception but passed")
            } catch RatesResponseError.invalidJsonNotConvertable {
                XCTAssert(true)
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseMissingRatesJSON() {
        let promise = expectation(description: "Response is JSON without 'rates'")
        
        let response = "{\"base\":\"RUB\",\"date\":\"2018-09-06\"}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let _ = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                XCTFail("Expected exception but passed")
            } catch RatesResponseError.invalidJsonNoRates {
                XCTAssert(true)
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseMissingBaseJSON() {
        let promise = expectation(description: "Response is JSON without 'base'")
        
        let response = "{\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.0203,\"CAD\":0.0193,\"EUR\":0.012507,\"GBP\":0.0113,\"NZD\":0.0222,\"USD\":0.0146}}"
        stub(everything, http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let _ = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                XCTFail("Expected exception but passed")
            } catch RatesResponseError.invalidJsonNoBase {
                XCTAssert(true)
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponseRedirect() {
        let promise = expectation(description: "Response with redirection")
        
        let response = "{\"base\":\"RUB\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.0203,\"CAD\":0.0193,\"EUR\":0.012507,\"GBP\":0.0113,\"NZD\":0.0222,\"USD\":0.0146}}"
        let fakeUrl = "http://test.example/rates?base=RUB"
        
        stub(uri(fakeUrl), {(request: URLRequest) -> Response in
            let response = HTTPURLResponse(url: request.url!, statusCode: 303, httpVersion: "HTTP/1.1", headerFields: ["Location": self.url])!
            return .success(response, .noContent)
        })
        stub(uri(url), http(200, headers: headers, download: .content(response.data(using: .utf8)!)))
        
        let expectedRatesData = [
            CurrencyData(code: "AUD", rateToEuro: 1.6241, amount: 0.0203),
            CurrencyData(code: "CAD", rateToEuro: 1.5411, amount: 0.0193),
            CurrencyData(code: "EUR", rateToEuro: 1, amount: 0.012507),
            CurrencyData(code: "GBP", rateToEuro: 0.90254, amount: 0.0113),
            CurrencyData(code: "NZD", rateToEuro: 1.7717, amount: 0.0222),
            CurrencyData(code: "RUB", rateToEuro: 79.956, amount: 1),
            CurrencyData(code: "USD", rateToEuro: 1.169, amount: 0.0146),
        ]
        let expectedBaseCurrencyCode = "RUB"
        let within = 0.01

        Alamofire.request(fakeUrl).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let actualRates = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                
                expect(interestingCurrencyCode).to(match("RUB"))
                expect(interestingCurrencyAmount).to(beCloseTo(1, within: within))
                expect(actualRates.baseCurrencyCode).to(match(expectedBaseCurrencyCode))
                expect(actualRates.ratesData).to(haveCount(expectedRatesData.count))
                for expectedCurrencyData in expectedRatesData {
                    expect(actualRates.ratesData).to(containElementSatisfying({currencyData in
                        if (expectedCurrencyData.code == currencyData.code) {
                            print("Delta for \(currencyData.code): \(abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro))")
                        }
                        return
                            (expectedCurrencyData.code == currencyData.code)
                            && (abs(expectedCurrencyData.rateToEuro - currencyData.rateToEuro) <= within)
                            && (abs(expectedCurrencyData.amount - currencyData.amount) <= within)
                    }))
                }
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testResponse404() {
        let promise = expectation(description: "Response 404 Error")
        
        stub(everything, http(404, headers: nil, download: .noContent))
        
        Alamofire.request(url).responseJSON { response in
            do {
                var interestingCurrencyCode: String?
                var interestingCurrencyAmount: Double?
                let _ = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &interestingCurrencyCode,
                    interestingCurrencyAmount: &interestingCurrencyAmount,
                    oldRatesData: nil,
                    oldRatesIndexMap: nil
                )
                XCTFail("Expected exception but passed")
            } catch {
                XCTAssert(true)
            }
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

}
