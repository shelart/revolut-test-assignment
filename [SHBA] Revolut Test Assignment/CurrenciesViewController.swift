//
//  TableViewController.swift
//  [SHBA] Revolut Test Assignment
//
//  Created by Artur Shelekhov-Balchunas on 16/10/2018.
//  Copyright © 2018 Artur Shelekhov-Balchunas. All rights reserved.
//

import UIKit
import Alamofire

class CurrenciesViewController: UITableViewController {
    
    var ratesInfo: RatesInfo?
    var timer: Timer?
    var interestingCurrencyCode: String? // the row which's UITextField is controlled by a user
    var interestingCurrencyRow: Int? // index of row of the above described
    var interestingCurrencyAmount: Double? // entered amount
    var amountFormatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.amountFormatter.minimumIntegerDigits = 1
        self.amountFormatter.minimumFractionDigits = 0
        self.amountFormatter.maximumFractionDigits = 4
        
        NotificationCenter.default.addObserver(self, selector: #selector(setupInterestingCurrency(notification:)), name: NSNotification.Name.interestedCurrency, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateAmounts(notification:)), name: NSNotification.Name.recalculateAmounts, object: nil)

        refreshRates()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = ratesInfo {
            return ratesInfo!.ratesData.count
        } else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> CurrencyCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currencyCell", for: indexPath) as! CurrencyCell

        // Configure the cell...
        cell.currencyTitle.text = ratesInfo!.ratesData[indexPath.row].code
        cell.currencyAmount.text = amountFormatter.string(from: NSNumber(value: ratesInfo!.ratesData[indexPath.row].amount))

        return cell
    }
    
    // MARK: - Process selecting of rows
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CurrencyCell
        NotificationCenter.default.post(name: Notification.Name.interestedCurrency, object: self, userInfo: [
            "code": cell.currencyTitle.text!
        ])
        cell.currencyAmount.becomeFirstResponder()
    }
    
    @objc func setupInterestingCurrency(notification: Notification) {
        self.interestingCurrencyCode = (notification.userInfo!["code"]! as? String)!
        self.interestingCurrencyRow = ratesInfo!.ratesIndexMap[self.interestingCurrencyCode!]!
        self.interestingCurrencyAmount = ratesInfo!.ratesData[self.interestingCurrencyRow!].amount
    }
    
    // MARK: - Process changing amount
    
    @objc func updateAmounts(notification: Notification) {
        self.interestingCurrencyAmount = (notification.userInfo!["newAmount"]! as! Double)
        self.ratesInfo!.ratesData[self.interestingCurrencyRow!].amount = self.interestingCurrencyAmount!
        RatesResponseHandler.calcAllAmounts(ratesData: &self.ratesInfo!.ratesData, interestingRowIdx: self.interestingCurrencyRow!)
        self.softlyUpdateRows()
    }
    
    func softlyUpdateRows() {
        var rowsToUpdate = self.tableView.indexPathsForVisibleRows ?? []
        let idxOfInterestingCurrency = self.ratesInfo!.ratesIndexMap[self.interestingCurrencyCode!]!
        for (idx, indexPath) in rowsToUpdate.enumerated() {
            if indexPath.row == idxOfInterestingCurrency {
                rowsToUpdate.remove(at: idx)
                break
            }
        }
        
        //self.tableView.beginUpdates()
        self.tableView.reloadRows(at: rowsToUpdate, with: UITableView.RowAnimation.none)
        //self.tableView.endUpdates()
    }
    
    // MARK: - Timer setup
    
    func scheduleRefresh() {
        unscheduleRefresh()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(refreshRates), userInfo: nil, repeats: false)
    }
    
    func unscheduleRefresh() {
        if let t = timer {
            t.invalidate()
        }
        timer = nil
    }

    // MARK: - Refresh exchange rates
    
    @objc func refreshRates() {
        unscheduleRefresh()
        Alamofire.request("https://revolut.duckdns.org/latest?base=EUR").responseJSON { response in
            do {
                let oldRatesInfo = self.ratesInfo
                var needFullReload = false
                self.ratesInfo = try RatesResponseHandler.processRatesResponse(
                    response: response,
                    interestingCurrencyCode: &self.interestingCurrencyCode,
                    interestingCurrencyAmount: &self.interestingCurrencyAmount,
                    oldRatesData: self.ratesInfo?.ratesData,
                    oldRatesIndexMap: self.ratesInfo?.ratesIndexMap
                )
                
                if oldRatesInfo == nil {
                    needFullReload = true
                } else {
                    if oldRatesInfo!.ratesIndexMap.count != self.ratesInfo!.ratesIndexMap.count {
                        needFullReload = true
                    } else {
                        for (currencyCode, currencyIdx) in self.ratesInfo!.ratesIndexMap {
                            if oldRatesInfo!.ratesIndexMap[currencyCode] != currencyIdx {
                                needFullReload = true
                                break
                            }
                        }
                    }
                }
                
                if needFullReload {
                    var wasFirstResponder = false
                    for cell in self.tableView.visibleCells {
                        if let castedCell = cell as? CurrencyCell {
                            if castedCell.currencyAmount.isFirstResponder {
                                wasFirstResponder = true
                                break
                            }
                        }
                    }
                    
                    self.tableView.reloadData()
                    
                    if wasFirstResponder {
                        let idxOfInterestingCurrency = self.ratesInfo!.ratesIndexMap[self.interestingCurrencyCode!]!
                        let indexPath = IndexPath(row: idxOfInterestingCurrency, section: 0)
                        
                        if let cell = self.tableView.cellForRow(at: indexPath) as? CurrencyCell {
                            cell.currencyAmount.becomeFirstResponder()
                        } else {
                            self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: false)
                            if let cell = self.tableView.cellForRow(at: indexPath) as? CurrencyCell {
                                cell.currencyAmount.becomeFirstResponder()
                            } else {
                                let deadlineTime = DispatchTime.now() + .seconds(1)
                                DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
                                    if let cell = self.tableView.cellForRow(at: indexPath) as? CurrencyCell {
                                        cell.currencyAmount.becomeFirstResponder()
                                    } else {
                                        print("Couldn't reactivate first responder")
                                    }
                                })
                            }
                        }
                    }
                } else {
                    self.softlyUpdateRows()
                }
            } catch RatesError.noEuroRate {
                print("Neither EUR rate exists, nor EUR is base currency!")
            } catch RatesError.dupBaseWithWrongRate(let base, let actualRate) {
                print("Base \(base) also presents in rates with rate \(actualRate)")
            } catch RatesResponseError.invalidJsonNoRates(let json) {
                print("Invalid JSON - no 'rates': \(json)")
            } catch RatesResponseError.invalidJsonNoBase(let json) {
                print("Invalid JSON - no 'base': \(json)")
            } catch RatesResponseError.invalidJsonNotConvertable(let utf8Text) {
                print("Invalid JSON - not convertable: \(utf8Text)")
            } catch RatesResponseError.invalidResponse(let response) {
                print("Invalid response: \(response)")
            } catch {
                print("Unexpected error: \(error)")
            }
            
            self.scheduleRefresh()
        }
    }

}
