//
//  RatesResponseHandler.swift
//  [SHBA] Revolut Test Assignment
//
//  Created by Artur Shelekhov-Balchunas on 16/10/2018.
//  Copyright © 2018 Artur Shelekhov-Balchunas. All rights reserved.
//

import Foundation
import Alamofire

enum RatesError: Error {
    case noEuroRate
    case dupBaseWithWrongRate(base: String, actualRate: Double)
}

enum RatesResponseError: Error {
    case invalidJsonNoRates(json: [String: Any])
    case invalidJsonNoBase(json: [String: Any])
    case invalidJsonNotConvertable(utf8Text: String)
    case invalidResponse(response: Alamofire.DataResponse<Any>)
}

struct CurrencyData {
    var code: String
    var rateToEuro: Double
    var amount: Double
}

struct RatesInfo {
    var baseCurrencyCode: String
    var ratesData: [CurrencyData]
    var ratesIndexMap: [String: Int]
}

class RatesResponseHandler {
    class func handleRates(
        baseCurrencyCode: String,
        rates: [String: Double],
        interestingCurrencyCode: inout String!,
        interestingCurrencyAmount: inout Double!,
        oldRatesData _oldRatesData: [CurrencyData]!,
        oldRatesIndexMap _oldRatesIndexMap: [String: Int]!
    ) throws -> RatesInfo {
        let oldRatesData: [CurrencyData]
        if _oldRatesData == nil {
            oldRatesData = []
        } else {
            oldRatesData = _oldRatesData
        }
        let oldRatesIndexMap: [String: Int]
        if _oldRatesIndexMap == nil {
            oldRatesIndexMap = [:]
        } else {
            oldRatesIndexMap = _oldRatesIndexMap
        }

        var euroRateToBase: Double
        if baseCurrencyCode == "EUR" {
            euroRateToBase = 1
        } else if rates["EUR"] != nil {
            euroRateToBase = rates["EUR"]!
        } else {
            throw RatesError.noEuroRate
        }
        
        if interestingCurrencyCode == nil {
            interestingCurrencyCode = baseCurrencyCode
        }
        
        if interestingCurrencyAmount == nil {
            interestingCurrencyAmount = 1
        }

        var ratesData: [CurrencyData] = []
        var ratesIndexMap: [String: Int] = [:]
        var idxOfBaseCurrency: Int?
        for (currencyCode, currencyRateToBase) in rates {
            let amount: Double
            if let oldCurrencyIdx = oldRatesIndexMap[currencyCode] {
                amount = oldRatesData[oldCurrencyIdx].amount
            } else {
                amount = 0
            }
            let currencyData = CurrencyData(code: currencyCode, rateToEuro: currencyRateToBase / euroRateToBase, amount: amount)
            ratesData.append(currencyData)
            if currencyCode == baseCurrencyCode {
                idxOfBaseCurrency = ratesData.count - 1 // to recover amount later
            }
        }
        
        if (rates[baseCurrencyCode] == nil) {
            // alright, base currency is not in rates dictionary
            ratesData.append(CurrencyData(code: baseCurrencyCode, rateToEuro: 1 / euroRateToBase, amount: interestingCurrencyAmount))
        } else if (rates[baseCurrencyCode] != 1) {
            throw RatesError.dupBaseWithWrongRate(base: baseCurrencyCode, actualRate: rates[baseCurrencyCode]!)
        } else {
            ratesData[idxOfBaseCurrency!].amount = interestingCurrencyAmount
        }
        
        ratesData.sort {(a, b) -> Bool in
            return a.code < b.code
        }
        
        for (idx, currencyData) in ratesData.enumerated() {
            ratesIndexMap[currencyData.code] = idx
        }
        
        let interestingCurrencyIdx = ratesIndexMap[interestingCurrencyCode]!
        self.calcAllAmounts(ratesData: &ratesData, interestingRowIdx: interestingCurrencyIdx)
        
        return RatesInfo(baseCurrencyCode: baseCurrencyCode, ratesData: ratesData, ratesIndexMap: ratesIndexMap)
    }
    
    class func processRatesResponse(
        response: Alamofire.DataResponse<Any>,
        interestingCurrencyCode: inout String!,
        interestingCurrencyAmount: inout Double!,
        oldRatesData: [CurrencyData]?,
        oldRatesIndexMap: [String: Int]?
    ) throws -> RatesInfo {
        if let json = response.result.value as! [String: Any]? {
            if let base = json["base"] as! String? {
                if let rates = json["rates"] as! [String: Double]? {
                    do {
                        return try self.handleRates(
                            baseCurrencyCode: base,
                            rates: rates,
                            interestingCurrencyCode: &interestingCurrencyCode,
                            interestingCurrencyAmount: &interestingCurrencyAmount,
                            oldRatesData: oldRatesData,
                            oldRatesIndexMap: oldRatesIndexMap
                        )
                    } catch {
                        throw error
                    }
                } else {
                    throw RatesResponseError.invalidJsonNoRates(json: json)
                }
            } else {
                throw RatesResponseError.invalidJsonNoBase(json: json)
            }
        } else if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
            throw RatesResponseError.invalidJsonNotConvertable(utf8Text: utf8Text)
        } else {
            throw RatesResponseError.invalidResponse(response: response)
        }
    }

    class func calcAllAmounts(ratesData: inout [CurrencyData], interestingRowIdx: Int) {
        for (idx, _) in ratesData.enumerated() {
            if (idx == interestingRowIdx) {
                continue
            }
            
            ratesData[idx].amount = self.calcAmount(ratesData: ratesData, forRowIdx: idx, interestingRowIdx: interestingRowIdx)
        }
    }

    class func calcAmount(ratesData: [CurrencyData], forRowIdx: Int, interestingRowIdx: Int) -> Double {
        let currentRateToInterestingRate = self.calcRate(ratesData: ratesData, forRowIdx: forRowIdx, interestingRowIdx: interestingRowIdx)
        let interestingAmount = ratesData[interestingRowIdx].amount
        return (interestingAmount * currentRateToInterestingRate)
    }

    class func calcRate(ratesData: [CurrencyData], forRowIdx: Int, interestingRowIdx: Int) -> Double {
        let currentRowRateToEuro = ratesData[forRowIdx].rateToEuro
        let interestingRateToEuro = ratesData[interestingRowIdx].rateToEuro
        return (currentRowRateToEuro / interestingRateToEuro)
    }
}
