# Test Assignment for Revolut #
## by Artur Shelekhov-Balchunas ##

This app shows currencies list. A user can enter amount of interested currency, and immediately get amount in other currencies
calculated by an each currency's exchange rate.
Exchange rates are dynamically updated each 1 second.

### Known issues: ###
1. The app cannot handle HTTP redirection. So, if the back-end API responds with a redirection, the app does not work.
2. If the back-end API changes currencies list (adding a currency or omitting previously responded currency), the keyboard will quickly jump,
and a text cursor will jump to the end of an entered amount.

### Deploying ###
1. Clone the repo

2. Go to a directory, into which the repo is cloned, and run:
 ```
 pod install
 ```
 You'll need [CocoaPods](https://cocoapods.org/) installed, of course.

3. Open `.xcworkspace` file in Xcode and attempt to run
